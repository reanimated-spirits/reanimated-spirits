function Events()
{
    /**
    * Moves the camera back and forth when you scroll the wheel
    */
    document.onwheel = function(e)
    {
        e = e || window.event;

        var tempCameraPosition = new THREE.Vector3();
        var sizeChanged = true;

        tempCameraPosition.set(camera.position.x, camera.position.y, camera.position.z);

        tempCameraPosition.z -= (e.wheelDelta * 0.1);

        if (tempCameraPosition.z < 1) {
            tempCameraPosition.z = 1;
            sizeChanged = false;
        }

        if (tempCameraPosition.z > 175) {
            tempCameraPosition.z = 175;
            sizeChanged = false;
        }

        if (sizeChanged) {
            camera.position.set(tempCameraPosition.x, tempCameraPosition.y, tempCameraPosition.z);

            renderer.render(scene, camera, sceneTexture, true);

            screenLeftTop = project2dPointToWorld(new THREE.Vector3(-1, 1, 0.5));
            screenRightBottom = project2dPointToWorld(new THREE.Vector3(1, -1, 0.5));

            isResizing = true;
        }
    }

    /**
    * Manages anything when the window resizes
    */
    window.onresize = function(e)
    {
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();

        renderer.setSize( window.innerWidth, window.innerHeight );

        isResizing = true;
    }

    /**
    * Tracks the mouse position
    */
    document.onmousemove = function(e)
    {
        e = e || window.event;

        var v = new THREE.Vector3(
            (e.clientX / window.innerWidth) * 2 - 1,
            -(e.clientY / window.innerHeight) * 2 + 1,
            0.5);

        mousePosition = project2dPointToWorld(v);
    }

    /**
    * When the mouse is held down convert the click to the world
    */
    document.onmousedown = function(e)
    {
        e = e || window.event;

        var v = new THREE.Vector3(
            (e.clientX / window.innerWidth) * 2 - 1,
            -(e.clientY / window.innerHeight) * 2 + 1,
            0.5);

        mousePosition = project2dPointToWorld(v);

        isMouseDown = true;
    }


    document.onmouseup = function(e)
    {
        e = e || window.event;

        isMouseDown = false;
    }

    /**
    * Disables the mouse right click
    */
    document.oncontextmenu = function(e)
    {
        e.preventDefault();
    }


}
