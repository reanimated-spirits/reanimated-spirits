var maxParticleCount = 15000;
var startingParticleCount = 500;

var instances = maxParticleCount;
var activeInstances = maxParticleCount;

var particleSpeed = 1;

var app, scene = null, finalScene = null, bloomScene, bloomSceneP2,
    camera = null, finalCamera = null, bloomCamera, bloomCameraP2, bloomSceneP2,
    sceneTexture = null, bloomTexture = null, bloomTextureP2,
    vsBloom1 = "", fsBloom1 = "", vsBloom2 = "", fsBloom2 = "", vsParticles = "", fsParticles = "", vsFullscreenQuad = "", fsFullscreenQuad = "",
    particleGeometry = null,
    renderer = null,
    particle = null,
    maxVelocity = 5.0,
    isMouseDown = false,
    mousePosition = null,
    screenLeftTop = null,
    screenRightBottom = null,
    isResizing = true;


InitScene();
Render();
Events();

/**
* Inititalizes the scene
*/
function InitScene() {


    vsBloom1 =  document.getElementById('bloom-p1-vs').textContent;
    fsBloom1 =  document.getElementById('bloom-p1-fs').textContent;
    vsBloom2 =  document.getElementById('bloom-p2-vs').textContent;
    fsBloom2 =  document.getElementById('bloom-p2-fs').textContent;
    vsFullscreenQuad =  document.getElementById('fullscreen-quad-vs').textContent;
    fsFullscreenQuad =  document.getElementById('fullscreen-quad-fs').textContent;
    vsParticles =  document.getElementById('particles-vs').textContent;
    fsParticles =  document.getElementById('particles-fs').textContent;


    scene = new THREE.Scene();
    finalScene = new THREE.Scene();
    bloomScene = new THREE.Scene();
    bloomSceneP2 = new THREE.Scene();

    renderer = new THREE.WebGLRenderer({ alpha: true });

    renderer.setClearColor( 0x040004, 1 );

    sceneTexture = new THREE.WebGLRenderTarget(window.innerWidth, window.innerHeight, { minFilter: THREE.LinearFilter, magFilter: THREE.NearestFilter, format: THREE.RGBFormat });
    bloomTexture = new THREE.WebGLRenderTarget(window.innerWidth, window.innerHeight, { minFilter: THREE.LinearFilter, magFilter: THREE.NearestFilter, format: THREE.RGBFormat });
    bloomTextureP2 = new THREE.WebGLRenderTarget(window.innerWidth, window.innerHeight, { minFilter: THREE.LinearFilter, magFilter: THREE.NearestFilter, format: THREE.RGBFormat });

    renderer.setSize(window.innerWidth, window.innerHeight);

    app = document.getElementById('app');
    app.appendChild( renderer.domElement );

    // main camera
    camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 1000);
    camera.position.z = 100;
    camera.position.y = 0;
    camera.lookAt(new THREE.Vector3(0,0,0));

    // render target camera
    finalCamera = new THREE.OrthographicCamera(-0.5, 0.5, 0.5, -0.5, 1, 1000);
    finalCamera.position.z = 1;

    bloomCamera = new THREE.OrthographicCamera(-0.5, 0.5, 0.5, -0.5, 1, 1000);
    bloomCamera.position.z = 1;

    bloomCameraP2 = new THREE.OrthographicCamera(-0.5, 0.5, 0.5, -0.5, 1, 1000);
    bloomCameraP2.position.z = 1;

    var geometry = new THREE.InstancedBufferGeometry();
    geometry.maxInstancedCount = instances;

    var verticeArray = [
        -1, 1, 0,
        1, 1, 0,
        -1, -1, 0,
        1, -1, 0,
    ];

    var indicesArray = [
        0, 1, 2,
        2, 1, 3,
    ];

    var vertices = new THREE.BufferAttribute(new Float32Array(verticeArray), 3);
    var indices = new Uint16Array(indicesArray);

    geometry.addAttribute('position', vertices);

    /**
    * Direction attributes - the direction that the particle is moving in
    */
    var directions = new THREE.InstancedBufferAttribute(new Float32Array(instances * 3), 3, 1);

    for ( var i = 0, count = directions.count; i < count; i++ ) {
        var v = new THREE.Vector3(0, 0, 0);

        /**
        * Creates a random direction whilst mantaining a certain speed
        */
        v = new THREE.Vector3(Math.random() + 0.5, Math.random() + 0.5, 1).multiplyScalar(0.5);

        if (Math.random() * 2 - 1 < 0) v.x = -v.x;
        if (Math.random() * 2 - 1 < 0) v.y = -v.y;

        directions.setXYZ(i, v.x, v.y, v.z);
    }
    geometry.addAttribute('direction', directions);

    /**
    * Offset attributes - the position of the particle
    */
    var offsets = new THREE.InstancedBufferAttribute(new Float32Array(instances * 3), 3, 1);
    for ( var i = 0, count = offsets.count; i < count; i++ ) {
        offsets.setXYZ(i, Math.random() * 400 - 200, Math.random() * 400 - 200, 0);
    }

    geometry.addAttribute('offset', offsets);

    /**
    * Color attributes - color of the particles that usually vary from dark to light pink
    */
    var colors = new THREE.InstancedBufferAttribute(new Float32Array(instances * 4), 4, 1);
    for ( var i = 0, count = colors.count; i < count; i++ ) {
        var redColor = clamp(Math.random(), 0.5, 1.0);
        var blue = clamp(Math.random(), 0.1, 0.2);
        var green = clamp(Math.random(), 0.0, 0.3);

        colors.setXYZW(i, redColor, blue, 0.5, 0.00);
    }
    geometry.addAttribute('color', colors);

    /**
    * Rotation attributes - this is the speed on each axis that the particles are rotating on
    */
    var rotations = new THREE.InstancedBufferAttribute(new Float32Array(instances * 3), 3, 1);
    for ( var i = 0, count = rotations.count; i < count; i++ ) {
        rotations.setXYZW(i, Math.random(), Math.random(), Math.random(), Math.random());
    }
    geometry.addAttribute('rotation', rotations);

    /**
    * Alive flag attributes - tracks the particles life
    */
    var lives = new THREE.InstancedBufferAttribute(new Float32Array(instances * 1), 1, 1);
    for ( var i = 0, count = lives.count; i < count; i++ ) {
        lives.setX(i, 1);
    }
    geometry.addAttribute('live', lives);

    geometry.setIndex( new THREE.BufferAttribute( indices, 1 ) );

    /**
    * Particle shader - moves and rotates the particles
    */
    var material = new THREE.ShaderMaterial({
        uniforms: {
            time: { type: 'f', value: 1.0 }
        },
        vertexShader: vsParticles,
        fragmentShader: fsParticles,
        side: THREE.DoubleSide,
        transparent: true
    });

    particle = new THREE.Mesh(geometry, material);

    scene.add(particle);


    screenLeftTop = project2dPointToWorld(new THREE.Vector3(-1, 1, 0.5));
    screenRightBottom = project2dPointToWorld(new THREE.Vector3(1, -1, 0.5));

    /**
    * Bloom pass 1 - this blurs everything horizontally
    */
    var geometry = new THREE.PlaneBufferGeometry(1, 1);

    var material = new THREE.ShaderMaterial({
        uniforms: {
            sceneTexture: { type: 't', value: sceneTexture },
            cameraDistance: { type: 'f', value: 0.0 }
        },
        vertexShader: vsBloom1,
        fragmentShader: fsBloom1,
    });

    var mesh = new THREE.Mesh(geometry, material);

    bloomScene.add(mesh);

    /**
    * Bloom pass 2 - this then blurs the horzontally blurred texture and blurs it vertically
    * You get a much more convincing glow when you do it in 2 passes rather than 1
    */
    var geometry = new THREE.PlaneBufferGeometry(1, 1);

    var material = new THREE.ShaderMaterial({
        uniforms: {
            bloomTexture: { type: 't', value: bloomTexture },
            sceneTexture: { type: 't', value: sceneTexture },
            cameraDistance: { type: 'f', value: 0.0 }
        },
        vertexShader: vsBloom2,
        fragmentShader: fsBloom2,
    });

    var mesh = new THREE.Mesh(geometry, material);

    bloomSceneP2.add(mesh);

    /**
    * Creates a full screen quad - this is the final render
    */
    var geometry = new THREE.PlaneBufferGeometry(1, 1);

    var material = new THREE.ShaderMaterial({
        uniforms: {
            sceneTexture: { type: "t", value: sceneTexture },
            bloomTexture: { type: "t", value: bloomTextureP2 }
        },
        vertexShader: vsFullscreenQuad,
        fragmentShader: fsFullscreenQuad,
    });

    var mesh = new THREE.Mesh(geometry, material);

    finalScene.add(mesh);

    instances = 1;
    activeInstances = startingParticleCount;

    camera.lookAt(new THREE.Vector3(0,0,0));
}

/**
* Render the scene
*/
function Render() {
    requestAnimationFrame(Render);

    if (instances < activeInstances) {
        instances += 20;
    }

    var time = performance.now();
    var object = scene.children[0];
    object.material.uniforms.time.value = time;

    var zoomDistance = (camera.position.z / 175) * 100;
    zoomDistance *= 0.01;
    zoomDistance *= 0.0011;

    var object = bloomScene.children[0];
    object.material.uniforms.cameraDistance.value = zoomDistance;

    var zoomDistance2 = (camera.position.z / 175) * 100;
    zoomDistance2 *= 0.01;
    zoomDistance2 *= 0.0004;

    var object = bloomSceneP2.children[0];
    object.material.uniforms.cameraDistance.value = zoomDistance2;

    var positions = particle.geometry.attributes.offset;
    var directions = particle.geometry.attributes.direction;
    var lives = particle.geometry.attributes.live;
    var colors = particle.geometry.attributes.color;


    /**
    * Speed up and slow down the particles depending on if the mouse is held down or released
    */
    if (isMouseDown && particleSpeed < maxVelocity) {
        particleSpeed += 0.01;

        if (particleSpeed > maxVelocity)
        {
            particleSpeed = maxVelocity;
        }
    } else {
        if (particleSpeed > 1) {
            particleSpeed -= 0.01;
        } else {
            particleSpeed = 1;
        }
    }

    /**
    * Loop over all of the instances that are active
    */
    for (var i = 0; i < instances; i++) {
        var index = i * 3;

        var alpha = colors.array[i * 4 + 3];
        var isAlive = lives.array[i];

        if (isAlive) {
            /**
            * Grabs the position & direction but put it in a more readable format
            */
            var particlePosition = new THREE.Vector3(
                positions.array[index],
                positions.array[index + 1],
                positions.array[index + 2]
            );

            var particleDirection = new THREE.Vector3(
                directions.array[index],
                directions.array[index + 1],
                directions.array[index + 2]
            );

            /**
            * If the mouse is held down then slowly begin to rotate the particle
            */
            if (isMouseDown && mousePosition) {
                var mouseDirection = mousePosition.clone().sub(particlePosition).normalize();

                var angle = getAngleBetweenVectors(particleDirection, mouseDirection);

                angle /= 10 + (Math.random() * 50);

                var r = new THREE.Matrix4().makeRotationZ(angle);
                particleDirection.applyMatrix4(r);

                directions.setXYZ(i, particleDirection.x, particleDirection.y, particleDirection.z);
            }

            particlePosition.x += particleDirection.x * particleSpeed;
            particlePosition.y += particleDirection.y * particleSpeed;

            if (isResizing) {
                if (particlePosition.x > screenRightBottom.x) lives.setX(i, 0);
                if (particlePosition.x < screenLeftTop.x) lives.setX(i, 0);
                if (particlePosition.y < screenRightBottom.y) lives.setX(i, 0);
                if (particlePosition.y > screenLeftTop.y) lives.setX(i, 0);
            } else {
                if (particlePosition.x > screenRightBottom.x) particlePosition.x = screenLeftTop.x;
                if (particlePosition.x < screenLeftTop.x) particlePosition.x = screenRightBottom.x;
                if (particlePosition.y < screenRightBottom.y) particlePosition.y = screenLeftTop.y;
                if (particlePosition.y > screenLeftTop.y) particlePosition.y = screenRightBottom.y;
            }

            positions.setXYZ(i, particlePosition.x, particlePosition.y, particlePosition.z);

            /**
            * If the user has lowered the number of particles on the screen then slowly begin to fade them out
            */
            if (i < activeInstances) {
                if (alpha <= 1.0) {
                    var speedIncrease = 0.01;

                    colors.setW(i, alpha + speedIncrease);
                }
            } else {
                if (isAlive) {
                    if (alpha >= 0.0) {
                        var speedIncrease = 0.01;

                        colors.setW(i, alpha - speedIncrease);
                    } else {
                        lives.setX(i, 0);
                        instances--;
                    }
                }
            }
        } else {
            if (i <= activeInstances) {
                lives.setX(i, 1);
            }

            if (isResizing) {
                lives.setX(i, 1);
                colors.setW(i, 0.0);
                positions.setXYZ(i, 0, 0, 0);
            }
        }
    }

    positions.needsUpdate = true;
    directions.needsUpdate = true;
    lives.needsUpdate = true;
    colors.needsUpdate = true;

    renderer.render(scene, camera, sceneTexture, true);
    renderer.render(bloomScene, bloomCamera, bloomTexture, true);
    renderer.render(bloomSceneP2, bloomCameraP2, bloomTextureP2, true);
    renderer.render(finalScene, finalCamera);

    /**
    * If the window screen has been resized then recalculate the boundaries
    */
    if (isResizing) {
        screenLeftTop = project2dPointToWorld(new THREE.Vector3(-1, 1, 0.5));
        screenRightBottom = project2dPointToWorld(new THREE.Vector3(1, -1, 0.5));
        isResizing = false;
    }
}
