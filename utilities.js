function project3dPointToScreen(vector)
{
    vector.project(camera);
    vector.x = Math.round((vector.x + 1) * renderer.domElement.width  / 2);
    vector.y = Math.round((-vector.y + 1) * renderer.domElement.height / 2);
    vector.z = 0;

    return vector;
}

function project2dPointToWorld(point)
{
    point.unproject(camera);

    var direction = point.sub(camera.position).normalize();
    var distance = -camera.position.z / direction.z;

    var pointWorldPosition = camera.position.clone().add(direction.multiplyScalar(distance));

    return pointWorldPosition;
}

function project2dPointToWorldY(point)
{
    point.unproject(camera);

    var direction = point.sub(camera.position).normalize();
    var distance = -camera.position.y / direction.y;

    var pointWorldPosition = camera.position.clone().add(direction.multiplyScalar(distance));

    return pointWorldPosition;
}

function getAngleBetweenVectors(a, b)
{
    var c = a.x * b.x + a.y * b.y;
    var d = a.x * b.y - a.y * b.x;
    var angle = Math.atan2(d, c);

    return angle;
}

function clamp(value, min, max) {
    if (value > max) value = max;
    if (value < min) value = min;

    return value;
}

function loadResource(path)
{
    var _this = this;

    return new Promise(function(resolve, reject)
    {
        var http = new XMLHttpRequest();

        http.onreadystatechange = function() {
            if (http.readyState == 4 && http.status == 200) {
                resolve(http.responseText);
            }
        };

        http.open("GET", path, true);
        http.send();
    });
}

function sinc(x) {
    if (x === 0) {
        return 1;
    } else {
        return Math.sin(x) / x;
    }
}
