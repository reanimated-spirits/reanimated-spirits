var app = null, rtScene, dataScene, ripplePointLength, Ripple, scene = null,
    camera = null,
    renderer = null,
    ocean = null,
    cube = null,
    rippleCount = 0,
    ripplePoints = [],
    mousePosition = new THREE.Vector3(0,0,0),
    cameraRotation = 0, rotateLeft = false, rotateRight = false, moveUpwards = false, moveDownwards = false,
    width = 100, height = 100, maxHeight = 1;

var controls;

init();
render();

function init()
{
    app = document.getElementById('app');

    scene = new THREE.Scene();
    rtScene = new THREE.Scene();
    dataScene = new THREE.Scene();

    camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 1000);
    camera.position.z = 10;
    camera.position.y = 50;

    var material = new THREE.RawShaderMaterial({
        uniforms: {
            time: { value: 0.0 },
            cameraPosition: { value: new THREE.Vector3() }
        },
        vertexShader: document.getElementById('vertex-shader').textContent,
        fragmentShader: document.getElementById('fragment-shader').textContent,
        side: THREE.DoubleSide,
        //wireframe: true,
    });

    var geometry = new THREE.PlaneBufferGeometry(200, 200, width, height);

    var positionOffsets = new THREE.BufferAttribute(new Float32Array(width * height * 4), 3);
    for (var i = 0, count = positionOffsets.count; i < count; i++) {
        positionOffsets.setXYZ(i, Math.random() * 2 - 1, Math.random() * 2 - 1, Math.random() * 2 - 1);
    }
    geometry.addAttribute('positionOffset', positionOffsets);

    ocean = new THREE.Mesh(geometry, material);
    ocean.rotateX(-Math.PI / 2);
    ocean.updateMatrix();
    ocean.geometry.applyMatrix(ocean.matrix);
    ocean.matrix.identity();
    ocean.geometry.verticesNeedUpdate = true;
    ocean.position.set( 0, 0, 0 );
    ocean.rotation.set( 0, 0, 0 );
    ocean.scale.set( 1, 1, 1 );

    scene.add(ocean);

    renderer = new THREE.WebGLRenderer( { alpha: true } );
    renderer.setClearColor( 0xffffff, 0 );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize(window.innerWidth, window.innerHeight);

    app.appendChild( renderer.domElement );
}

var count = 0;

function render() {

    requestAnimationFrame(render);

    var time = performance.now();
    var obj = scene.children[0];
    obj.material.uniforms.time.value = time;
    obj.material.uniforms.cameraPosition.value = camera.position;

    var offsetPositions = ocean.geometry.attributes.positionOffset;

    var positions = ocean.geometry.attributes.position;

    var vertexCount = width * height;

    ripplePointLength = ripplePoints.length;

    for (var j = 0; j < ripplePointLength; j++) {
        if (ripplePoints[j]) {
            if (ripplePoints[j].life <= 0) {
                ripplePoints.splice(i, 1);
            } else {
                ripplePoints[j].life--;
                ripplePoints[j].speed += 50;
            }
        }
    }

    ripplePointLength = ripplePoints.length;

    for (var i = 0, length = positions.count; i < length; i++) {
        var index = i * 3;
        var currentVertex = new THREE.Vector3(positions.array[index], positions.array[index + 1], positions.array[index + 2]);
        var newPosition = new THREE.Vector3(
            positions.array[index],
            Math.sin(time * 0.002 + offsetPositions.array[index + 1] ) * offsetPositions.array[index] * 0.6,
            positions.array[index + 2]);

        for (var j = 0; j < ripplePointLength; j++) {
            var distance = currentVertex.distanceTo(ripplePoints[j].spawnPoint);

            if (distance < 20) {
                var height = sinc(distance - ripplePoints[j].speed * 0.006) * ripplePoints[j].getLifePercent() / 40;
                newPosition.y += height;

                if (newPosition.y > maxHeight) {
                    newPosition.y = maxHeight;
                }
            }
        }

        positions.setXYZ(i, newPosition.x, newPosition.y, newPosition.z);
    }

    offsetPositions.needsUpdate = true;
    positions.needsUpdate = true;

    ocean.geometry.computeVertexNormals();

    renderer.clear();

    cameraMovement();

    renderer.render(scene, camera);
}

function cameraMovement()
{
    if (rotateRight) {
        cameraRotation += 0.02;
    }

    if (rotateLeft) {
        cameraRotation -= 0.02;
    }

    if (moveUpwards) {
        if (camera.position.y < 200) {
            camera.position.y += 3;
        } else {
            camera.position.y = 200;
        }
    }

    if (moveDownwards) {
        if (camera.position.y > 10) {
            camera.position.y -= 3;
        } else {
            camera.position.y = 10;
        }
    }

    camera.position.x = 100 * Math.cos(cameraRotation);
    camera.position.z = 100 * Math.sin(cameraRotation);

    camera.lookAt(new THREE.Vector3(0,0,0));
}
