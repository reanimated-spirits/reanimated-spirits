document.onkeyup = function(e)
{
    e = e || window.event;

    if (e.keyCode == '65') {
        rotateRight = false;
    }

    if (e.keyCode == '68') {
        rotateLeft = false;
    }

    if (e.keyCode == '83') {
        moveDownwards = false;
    }

    if (e.keyCode == '87') {
        moveUpwards = false;
    }
}

document.onkeydown = function(e)
{
    e = e || window.event;

    if (e.keyCode == '83') {
        moveDownwards = true;
    }
    else if (e.keyCode == '87') {
        moveUpwards = true;
    }

    if (e.keyCode == '65') {
        rotateRight = true;
    }

    if (e.keyCode == '68') {
        rotateLeft = true;
    }
}

window.onresize = function(e)
{
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize( window.innerWidth, window.innerHeight );
}

document.onmousedown = function(e)
{
    e = e || window.event;

    var v = new THREE.Vector3(
        (e.clientX / window.innerWidth) * 2 - 1,
        -(e.clientY / window.innerHeight) * 2 + 1,
        0.5);

    mousePosition = project2dPointToWorldY(v);

    rippleCount = 0;

    var ripple = new Ripple(mousePosition);

    ripplePoints.push(ripple);
}
