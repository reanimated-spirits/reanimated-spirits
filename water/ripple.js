Ripple = function(spawn) {
    this.spawnPoint = spawn;
    this.maxLife = 60;
    this.life = this.maxLife;
    this.speed = 0;
};

Ripple.prototype.getLifePercent = function()
{
    return this.life / this.maxLife * 100;
}
